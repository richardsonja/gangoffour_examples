﻿using Gof.Factory.Creators;

namespace Gof.Factory.Factories
{
    public interface IFactory
    {
        ICreator GetCreator(CreatorType creatorType);
    }
}