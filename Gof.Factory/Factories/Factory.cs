﻿using System;
using Gof.Factory.Creators;

namespace Gof.Factory.Factories
{
    public class Factory : IFactory
    {
        public ICreator GetCreator(CreatorType creatorType)
        {
            return creatorType switch
            {
                CreatorType.Type1 => new ConcreteCreator1(),
                CreatorType.Type2 => new ConcreteCreator2(),
                _ => throw new ArgumentOutOfRangeException(nameof(creatorType))
            };
        }
    }
}