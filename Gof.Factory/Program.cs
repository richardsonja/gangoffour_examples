﻿using Microsoft.Extensions.DependencyInjection;
using System;
using Gof.Factory.Creators;
using Gof.Factory.Factories;

namespace Gof.Factory
{
    public class Program
    {
        static void Main()
        {
            //setup our DI
            var serviceProvider = new ServiceCollection()
                .AddTransient<IFactory, Factories.Factory>()
             .BuildServiceProvider();


            //do the actual work here
              var factory = serviceProvider.GetService<IFactory>();
              Output(factory.GetCreator(CreatorType.Type1));
              Output(factory.GetCreator(CreatorType.Type2));


        }

        public static void Output(ICreator creator)
        {
            // ...
            Console.WriteLine("Client: I'm not aware of the creator's class," +
                              "but it still works.\n" + creator.SomeOperation());
            // ...
        }
    }
}
