﻿namespace Gof.Factory.Products
{
    public class ConcreteProduct1 : IProduct
    {
        public string Operation()
        {
            return $"--Result of {nameof(ConcreteProduct1)}--";
        }
    }
}