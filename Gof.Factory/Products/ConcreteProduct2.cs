﻿namespace Gof.Factory.Products
{
    public class ConcreteProduct2 : IProduct
    {
        public string Operation()
        {
            return $"--Result of {nameof(ConcreteProduct2)}--";
        }
    }
}