﻿namespace Gof.Factory.Products
{
    public interface IProduct
    {
        string Operation();
    }
}