﻿using Gof.Factory.Products;

namespace Gof.Factory.Creators
{
    public class ConcreteCreator2 : IConcreteCreator2
    {
        public IProduct FactoryMethod()
        {
            return new ConcreteProduct2();
        }
    }
}