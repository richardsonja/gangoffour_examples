﻿using Gof.Factory.Products;

namespace Gof.Factory.Creators
{
    public interface ICreator
    {
        IProduct FactoryMethod();

        string SomeOperation()
        {
            // Call the factory method to create a Product object.
            var product = FactoryMethod();
            // Now, use the product.
            var result = "Creator: The same creator's code has just worked with "
                         + product.Operation();

            return result;
        }
    }
}