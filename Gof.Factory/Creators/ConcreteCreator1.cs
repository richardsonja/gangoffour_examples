﻿using Gof.Factory.Products;

namespace Gof.Factory.Creators
{
    public class ConcreteCreator1 : IConcreteCreator1
    {
        public IProduct FactoryMethod()
        {
            return new ConcreteProduct1();
        }
    }
}