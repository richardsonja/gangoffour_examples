﻿using System;
using Gof.Adapter.Adaptees;
using Gof.Adapter.Adapters;

namespace Gof.Adapter
{
    internal class Program
    {
        private static void Main()
        {
            var cup = new Cup(10, 10);
            var square = new Square(10, 10);

            DisplayMessage(new CupAdapter(cup));
            Console.WriteLine();
            DisplayMessage(new SquareAdapter(square));
        }

        private static void DisplayMessage<T>(IAdapter<T> adapter)
        {
            Console.WriteLine($"{adapter.Type()} example:");
            Console.WriteLine(adapter.GetMessage());
        }
    }
}