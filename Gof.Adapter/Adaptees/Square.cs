﻿namespace Gof.Adapter.Adaptees
{
    public class Square
    {
        private readonly int _height;
        private readonly int _width;

        public Square(int height, int width)
        {
            _height = height;
            _width = width;
        }

        public int Area()
        {
            return _height * _width;
        }

        public string SomeMessage()
        {
            return $"This square has an area of {Area()} inches.";
        }
    }
}