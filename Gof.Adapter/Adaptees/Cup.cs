﻿namespace Gof.Adapter.Adaptees
{
    public class Cup
    {
        private readonly int _height;
        private readonly int _radius;

        public Cup(int height, int radius)
        {
            _height = height;
            _radius = radius;
        }

        public string ExplainYourself()
        {
            return $"This is a cup with a volume of {Volume()}";
        }

        public double Volume()
        {
            const double pie = 3.14285714286;
            return pie * (_radius * _radius) * _height;
        }
    }
}