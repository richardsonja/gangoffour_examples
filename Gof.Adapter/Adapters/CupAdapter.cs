﻿using Gof.Adapter.Adaptees;

namespace Gof.Adapter.Adapters
{
    public class CupAdapter : IAdapter<Cup>
    {
        public CupAdapter(Cup adaptee)
        {
            Adaptee = adaptee;
        }

        public Cup Adaptee { get; private set; }

        public string GetMessage()
        {
            return Adaptee.ExplainYourself();
        }
    }
}