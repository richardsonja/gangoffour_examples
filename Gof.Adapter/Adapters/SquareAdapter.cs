﻿using Gof.Adapter.Adaptees;

namespace Gof.Adapter.Adapters
{
    public class SquareAdapter : IAdapter<Square>
    {
        public SquareAdapter(Square adaptee)
        {
            Adaptee = adaptee;
        }

        public Square Adaptee { get; private set; }

        public string GetMessage()
        {
            return Adaptee.SomeMessage();
        }
    }
}