﻿namespace Gof.Adapter.Adapters
{
    public interface IAdapter<out T>
    {
        T Adaptee { get; }
        string GetMessage();

        string Type()
        {
            return Adaptee.GetType().ToString();
        }
    }
}