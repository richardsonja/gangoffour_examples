﻿using Gof.Facade.Subsystems;

namespace Gof.Facade.Facades
{
    public class Facade
    {
        protected SubsystemA Subsystem1;

        protected SubsystemB Subsystem2;

        public Facade(SubsystemA subsystem1, SubsystemB subsystem2)
        {
            Subsystem1 = subsystem1;
            Subsystem2 = subsystem2;
        }

        // The Facade's methods are convenient shortcuts to the sophisticated
        // functionality of the subsystems. However, clients get only to a
        // fraction of a subsystem's capabilities.
        public string Operation()
        {
            var result = "Facade initializes subsystems:\n";
            result += Subsystem1.Operation1();
            result += Subsystem2.SomeOperation();
            result += "Facade orders subsystems to perform the action:\n";
            result += Subsystem1.OperationN();
            result += Subsystem2.AnotherOperation();
            return result;
        }
    }
}