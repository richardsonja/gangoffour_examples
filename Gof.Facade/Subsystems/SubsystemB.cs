﻿namespace Gof.Facade.Subsystems
{
    public class SubsystemB
    {
        public string SomeOperation()
        {
            return "SubsystemB: Ready!\n";
        }

        public string AnotherOperation()
        {
            return "SubsystemB: Go!\n";
        }
    }
}
