﻿namespace Gof.Facade.Subsystems
{
    public class SubsystemA
    {
        public string Operation1()
        {
            return "SubsystemA: Ready!\n";
        }

        public string OperationN()
        {
            return "SubsystemA: Go!\n";
        }
    }
}
