﻿using System;
using Gof.Facade.Subsystems;

namespace Gof.Facade
{
    class Program
    {
        static void Main()
        {
           ClientCode(new Facades.Facade(new SubsystemA(), new SubsystemB()));
        }

        public static void ClientCode(Facades.Facade facade)
        {
            Console.Write(facade.Operation());
        }
    }
}
