﻿using Gof.ChainOfResponsibility.Applied.Middlewares;
using System;
using System.Collections.Generic;

namespace Gof.ChainOfResponsibility.Applied.Servers
{
    public class Server : IServer
    {
        private readonly Dictionary<string, string> _users = new Dictionary<string, string>();
        private Middleware _middleware;

        public bool HasEmail(string email) => _users.ContainsKey(email);

        public bool IsValidPassword(string email, string password) => HasEmail(email)
                                                                      && _users[email] == password;

        public bool LogIn(string email, string password)
        {
            if (!_middleware.Check(email, password))
            {
                return false;
            }

            Console.WriteLine("Authorization has been successful!");
            return true;
        }

        public void Register(string email, string password)
        {
            if (HasEmail(email))
            {
                Console.WriteLine("Already registered.  Ignoring.");
            }

            _users.Add(email, password);
        }

        public void SetMiddleware(Middleware middleware)
        {
            _middleware = middleware;
        }
    }
}