﻿using Gof.ChainOfResponsibility.Applied.Middlewares;

namespace Gof.ChainOfResponsibility.Applied.Servers
{
    public interface IServer
    {
        bool HasEmail(string email);
        bool IsValidPassword(string email, string password);
        bool LogIn(string email, string password);
        void Register(string email, string password);
        void SetMiddleware(Middleware middleware);
    }
}