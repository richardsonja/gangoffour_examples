﻿using Gof.ChainOfResponsibility.Applied.Middlewares;
using Gof.ChainOfResponsibility.Applied.Servers;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.ComponentModel.DataAnnotations;

namespace Gof.ChainOfResponsibility.Applied
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            //setup our DI
            var serviceProvider = new ServiceCollection()
                                  .AddTransient<IServer, Server>()
                                  .BuildServiceProvider();

            //do the actual work here
            var server = serviceProvider.GetService<IServer>();
            server.Register("admin@example.com", "admin_pass");
            server.Register("user@example.com", "user_pass");

            var middleware = new ThrottlingMiddleware(2);
            middleware
                .LinkedWith(new UserExistsMiddleware(server))
                .LinkedWith(new RoleCheckMiddleware());

            server.SetMiddleware(middleware);

            ClientCode(server, "user@example.com", "user_pass");
            ClientCode(server, "admin@example.com", "admin_pass");
            ClientCode(server, "bob@example.com", "was here");
            ClientCode(server, "user@example.com", "WRO(NG");
            ClientCode(server, "admin@example.com", "admin_pass");
        }

        private static void ClientCode(IServer server, string email, string password)
        {
            try
            {
                Console.WriteLine(
                                  server.LogIn(email, password)
                                      ? "Not successful"
                                      : "INVALID");
            }
            catch (ValidationException e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}