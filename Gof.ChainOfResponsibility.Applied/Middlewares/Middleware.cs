﻿namespace Gof.ChainOfResponsibility.Applied.Middlewares
{
    public abstract class Middleware
    {
        private Middleware _next;

        public virtual bool Check(string email, string password) => _next == null || _next.Check(email, password);

        public Middleware LinkedWith(Middleware next)
        {
            _next = next;
            return next;
        }
    }
}