﻿using Gof.ChainOfResponsibility.Applied.Servers;
using System;

namespace Gof.ChainOfResponsibility.Applied.Middlewares
{
    public class UserExistsMiddleware : Middleware
    {
        private readonly IServer _server;

        public UserExistsMiddleware(IServer server) => _server = server;

        public override bool Check(string email, string password)
        {
            if (!_server.HasEmail(email))
            {
                Console.WriteLine("Email is not registered!");
                return false;
            }

            if (_server.IsValidPassword(email, password))
            {
                return base.Check(email, password);
            }

            // NOTE: both of these cases should return one message: "Username or Password is invalid"
            //          its good security practice to not tell the person which one was wrong.
            Console.WriteLine("Password is invalid!");
            return false;
        }
    }
}