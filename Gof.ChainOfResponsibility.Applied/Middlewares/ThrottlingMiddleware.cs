﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Gof.ChainOfResponsibility.Applied.Middlewares
{
    public class ThrottlingMiddleware : Middleware
    {
        private readonly int _requestPerMinute;

        private DateTime _currentDateTime;

        private int _request;

        public ThrottlingMiddleware(int requestPerMinute)
        {
            _requestPerMinute = requestPerMinute;
            _currentDateTime = DateTime.Now;
        }

        public override bool Check(string email, string password)
        {
            if (DateTime.Now > _currentDateTime.AddHours(60))
            {
                _request = 0;
                _currentDateTime = DateTime.Now;
            }

            _request++;

            if (_request <= _requestPerMinute)
            {
                return base.Check(email, password);
            }

            Console.WriteLine("Request Limit Exceeded!");
            throw new ValidationException("You are locked out.");
        }
    }
}