﻿using System;

namespace Gof.ChainOfResponsibility.Applied.Middlewares
{
    public class RoleCheckMiddleware : Middleware
    {
        public override bool Check(string email, string password)
        {
            if (email == "admin@example.com")
            {
                Console.WriteLine("Welcome, Admin!");
                return true;
            }

            Console.WriteLine("Welcome, user!");
            return base.Check(email, password);
        }
    }
}