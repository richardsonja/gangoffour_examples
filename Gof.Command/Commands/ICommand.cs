﻿namespace Gof.Command.Commands
{
    public interface ICommand
    {
        void Execute();
    }
}