﻿using Gof.Command.Commands;
using Gof.Command.Commands.Receivers;
using Gof.Command.Invokers;

namespace Gof.Command
{
    internal class Program
    {
        private static void Main()
        {
            var invoker = new Invoker();
            invoker.SetOnStart(new SimpleCommand("Say Hi!"));
            var receiver = new Receiver();
            invoker.SetOnFinish(new ComplexCommand(receiver, "Send email", "Save report"));

            invoker.DoSomethingImportant();
        }
    }
}