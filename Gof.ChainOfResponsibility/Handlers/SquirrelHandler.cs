﻿using Gof.ChainOfResponsibility.Animals;

namespace Gof.ChainOfResponsibility.Handlers
{
    public class SquirrelHandler : AbstractAnimalHandler
    {
        public SquirrelHandler() : base(new Squirrel())
        {
        }
    }
}