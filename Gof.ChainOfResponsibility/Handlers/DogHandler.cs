﻿using Gof.ChainOfResponsibility.Animals;

namespace Gof.ChainOfResponsibility.Handlers
{
    public class DogHandler : AbstractAnimalHandler
    {
        public DogHandler() : base(new Dog())
        {
        }
    }
}