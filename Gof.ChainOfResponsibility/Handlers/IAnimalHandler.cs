﻿namespace Gof.ChainOfResponsibility.Handlers
{
    public interface IAnimalHandler
    {
        IAnimalHandler SetNext(IAnimalHandler animalHandler);

        object Handle(Food request);
    }
}