﻿using Gof.ChainOfResponsibility.Animals;

namespace Gof.ChainOfResponsibility.Handlers
{
    public class MonkeyHandler : AbstractAnimalHandler
    {
        public MonkeyHandler() : base(new Monkey())
        {
        }
    }
}