﻿using Gof.ChainOfResponsibility.Animals;

namespace Gof.ChainOfResponsibility.Handlers
{
    public class AbstractAnimalHandler : IAnimalHandler
    {
        private readonly IAnimal _currentAnimal;
        private IAnimalHandler _nextAnimalHandler;

        protected AbstractAnimalHandler(IAnimal currentAnimal)
        {
            _currentAnimal = currentAnimal;
        }

        public IAnimalHandler SetNext(IAnimalHandler animalHandler)
        {
            _nextAnimalHandler = animalHandler;

            // Returning a animalHandler from here will let us link handlers in a
            // convenient way like this:
            // monkey.SetNext(squirrel).SetNext(dog);
            return animalHandler;
        }

        public virtual object Handle(Food request)
        {
            var isFavoriteFood = _currentAnimal.IsFavoriteFood(request);
            if (!isFavoriteFood && _nextAnimalHandler == null) return null;

            return isFavoriteFood
                ? _currentAnimal.Response()
                : _nextAnimalHandler.Handle(request);
        }
    }
}