﻿using System;
using System.Collections.Generic;
using Gof.ChainOfResponsibility.Handlers;

namespace Gof.ChainOfResponsibility
{
    internal class Program
    {
        private static void Main()
        {
            var monkey = new MonkeyHandler();
            var squirrel = new SquirrelHandler();
            var dog = new DogHandler();


            monkey.SetNext(squirrel).SetNext(dog);

            Console.WriteLine("Chain: Monkey > Squirrel > Dog\n");
            ClientCode(monkey);
            Console.WriteLine();


            Console.WriteLine("Subchain: Squirrel > Dog\n");
            ClientCode(squirrel);
        }

        public static void ClientCode(AbstractAnimalHandler handler)
        {
            var foodList = new List<Food>
            {
                Food.Nut,
                Food.Banana,
                (Food) 158,
                Food.MeatBall
            };

            foodList.ForEach(x => ClientCode(handler, x));
        }

        private static void ClientCode(AbstractAnimalHandler handler, Food food)
        {
            Console.WriteLine($"Client: Who wants a {food}?");
            var result = handler.Handle(food);

            Console.WriteLine(result != null
                ? $"   {result}"
                : $"   {food} was left untouched.\n");
        }
    }
}