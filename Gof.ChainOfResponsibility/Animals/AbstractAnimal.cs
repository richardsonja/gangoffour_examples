﻿namespace Gof.ChainOfResponsibility.Animals
{
    public abstract class AbstractAnimal : IAnimal
    {
        protected readonly Food FavoriteFood;

        protected AbstractAnimal(Food favoriteFood)
        {
            FavoriteFood = favoriteFood;
        }

        public bool IsFavoriteFood(Food request)
        {
            return request == FavoriteFood;
        }

        public abstract object Response();
    }
}