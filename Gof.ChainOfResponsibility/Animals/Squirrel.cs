﻿namespace Gof.ChainOfResponsibility.Animals
{
    public class Squirrel : AbstractAnimal
    {
        public Squirrel() : base(Food.Nut)
        {
        }

        public override object Response()
        {
            return $"Squirrel: MINE!.  This {FavoriteFood.ToString()} is MINE!\n";
        }
    }
}