﻿namespace Gof.ChainOfResponsibility.Animals
{
    public class Monkey : AbstractAnimal
    {
        public Monkey() : base(Food.Banana)
        {
        }

        public override object Response()
        {
            return $"I am Monkey: I'll eat the {FavoriteFood.ToString()}.\n";
        }
    }
}