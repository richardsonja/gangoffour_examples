﻿namespace Gof.ChainOfResponsibility.Animals
{
    public class Dog : AbstractAnimal
    {
        public Dog() : base(Food.MeatBall)
        {
        }

        public override object Response()
        {
            return $"Dog: Please...Please...I'll eat the {FavoriteFood.ToString()}.\n";
        }
    }
}