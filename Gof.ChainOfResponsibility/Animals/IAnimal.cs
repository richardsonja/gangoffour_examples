﻿namespace Gof.ChainOfResponsibility.Animals
{
    public interface IAnimal
    {
        bool IsFavoriteFood(Food request);

        object Response();
    }
}