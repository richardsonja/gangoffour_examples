﻿using Gof.Factory.Flyweight.Cars;
using System;
using System.Text.Json;

namespace Gof.Factory.Flyweight.Flyweights
{
    public class Flyweight
    {
        private readonly Car _sharedState;

        public Flyweight(Car car) => _sharedState = car;

        public void Operation(Car uniqueState)
        {
            var s = JsonSerializer.Serialize(_sharedState);
            var u = JsonSerializer.Serialize(uniqueState);
            Console.WriteLine($"Flyweight: Displaying shared {s} and unique {u} state.");
        }
    }
}