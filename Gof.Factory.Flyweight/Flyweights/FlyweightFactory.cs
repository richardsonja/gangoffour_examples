﻿using System;
using System.Collections.Generic;
using System.Linq;
using Gof.Factory.Flyweight.Cars;

namespace Gof.Factory.Flyweight.Flyweights
{
    public class FlyweightFactory
    {
        private readonly List<Tuple<Flyweight, string>> _flyweights = new List<Tuple<Flyweight, string>>();

        public FlyweightFactory(params Car[] args)
        {
            foreach (var elem in args)
            {
                _flyweights.Add(new Tuple<Flyweight, string>(new Flyweight(elem), this.GetKey(elem)));
            }
        }

        // Returns a Flyweight's string hash for a given state.
        public string GetKey(Car key)
        {
            var elements = new List<string> {key.Model, key.Color, key.Company};


            if (key.Owner != null && key.Number != null)
            {
                elements.Add(key.Number);
                elements.Add(key.Owner);
            }

            elements.Sort();

            return string.Join("_", elements);
        }

        // Returns an existing Flyweight with a given state or creates a new
        // one.
        public Flyweight GetFlyweight(Car sharedState)
        {
            var key = this.GetKey(sharedState);

            if (_flyweights.All(t => t.Item2 != key))
            {
                Console.WriteLine("FlyweightFactory: Can't find a flyweight, creating new one.");
                this._flyweights.Add(new Tuple<Flyweight, string>(new Flyweight(sharedState), key));
            }
            else
            {
                Console.WriteLine("FlyweightFactory: Reusing existing flyweight.");
            }

            return this._flyweights.FirstOrDefault(t => t.Item2 == key)?.Item1;
        }

        public void ListFlyweights()
        {
            var count = _flyweights.Count;
            Console.WriteLine($"\nFlyweightFactory: I have {count} flyweights:");
            foreach (var flyweight in _flyweights)
            {
                Console.WriteLine(flyweight.Item2);
            }
        }
    }
}