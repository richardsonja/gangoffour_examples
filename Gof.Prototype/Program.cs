﻿using System;
using Gof.Prototype.Models;

namespace Gof.Prototype
{
    internal class Program
    {
        private static void Main()
        {
            var personA = new Person
            {
                Age = 42,
                BirthDate = DateTime.Today.AddYears(-42),
                Name = "Some Dude",
                IdInfo = new IdInfo(1234)
            };

            Console.WriteLine("-----------Person A");
            Console.WriteLine(personA);

            var personB = personA.ShallowCopy();
            personB.IdInfo = new IdInfo(5678);
            Console.WriteLine("-----------Person B");
            Console.WriteLine(personB);
            personA.Age = 43;
            personA.BirthDate = DateTime.Today.AddYears(-43);
            personB.Age = 15;
            personB.BirthDate = DateTime.Today.AddYears(-15);
            personB.Name = "Some Other Dude";
            Console.WriteLine("------Person A");
            Console.WriteLine(personA);
            Console.WriteLine("------Person B");
            Console.WriteLine(personB);


            var personC = personA.DeepCopy();
            personC.IdInfo = new IdInfo(9264);
            Console.WriteLine("-----------Person C");
            Console.WriteLine(personC);
            personC.Age = 78;
            personC.BirthDate = DateTime.Today.AddYears(-78);
            personC.Name = "Some Cool Wise Dude";
            Console.WriteLine("------Person A");
            Console.WriteLine(personA);
            Console.WriteLine("------Person B");
            Console.WriteLine(personB);
            Console.WriteLine("------Person C");
            Console.WriteLine(personC);
        }
    }
}