﻿using System;

namespace Gof.Prototype.Models
{
    public class Person
    {
        public int Age;
        public DateTime BirthDate;
        public IdInfo IdInfo;
        public string Name;

        public Person ShallowCopy()
        {
            return (Person) MemberwiseClone();
        }

        public Person DeepCopy()
        {
            var clone = ShallowCopy();
            clone.IdInfo = new IdInfo(IdInfo.IdNumber);
            //clone.Name = string.Copy(Name);
            return clone;
        }

        public override string ToString()
        {
            return $"Id {IdInfo.IdNumber} - {Name} is {Age} years old and was born on {BirthDate.ToShortDateString()}";
        }
    }
}