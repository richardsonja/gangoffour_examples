﻿using Gof.Strategy.Strategies;

namespace Gof.Strategy.Contexts
{
    public interface IContext
    {
        void SetStrategy(IStrategy strategy);
        void DoSomeBusinessLogic();
    }
}