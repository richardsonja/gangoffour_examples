﻿namespace Gof.Strategy.Strategies
{
    public interface IStrategy
    {
        object DoAlgorithm(object data);
    }
}