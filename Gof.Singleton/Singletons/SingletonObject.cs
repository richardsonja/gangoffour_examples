﻿using System;

namespace Gof.Singleton.NotThreadSafe.Singletons
{
    public class SingletonObject : ISingletonObject
    {
        private SingletonObject() { }

        // The Singleton's instance is stored in a static field. There there are
        // multiple ways to initialize this field, all of them have various pros
        // and cons. In this example we'll show the simplest of these ways,
        // which, however, doesn't work really well in multithreaded program.
        private static SingletonObject _instance;

        // This is the static method that controls the access to the singleton
        // instance. On the first run, it creates a singleton object and places
        // it into the static field. On subsequent runs, it returns the client
        // existing object stored in the static field.
        public static SingletonObject GetInstance()
        {
            return _instance ??= new SingletonObject();
        }

        // Finally, any singleton should define some business logic, which can
        // be executed on its instance.
        public static void SomeBusinessLogic()
        {
            // ...
            Console.WriteLine("This is not thread safe");
        }

        public void SomeMessage()
        {
            // ...
            Console.WriteLine("This is not thread safe either");
        }
    }
}
