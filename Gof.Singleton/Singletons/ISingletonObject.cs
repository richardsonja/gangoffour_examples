﻿namespace Gof.Singleton.NotThreadSafe.Singletons
{
    public interface ISingletonObject
    {
        void SomeMessage();
    }
}