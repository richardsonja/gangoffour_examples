﻿using System;
using Gof.Singleton.NotThreadSafe.Singletons;

namespace Gof.Singleton.NotThreadSafe
{
    public class Program
    {
        public static void Main()
        {
            var s1 = SingletonObject.GetInstance();
            var s2 = SingletonObject.GetInstance();

            Console.WriteLine(s1 == s2
                ? "Singleton works, both variables contain the same instance."
                : "Singleton failed, variables contain different instances.");

            SingletonObject.SomeBusinessLogic();
            s1.SomeMessage();
        }
    }
}