﻿using Gof.Visitor.Visitors;

namespace Gof.Visitor.Components
{
    public interface IComponent
    {
        void Accept(IVisitor visitor);
    }
}