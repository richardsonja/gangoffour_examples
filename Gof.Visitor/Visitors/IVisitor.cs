﻿using Gof.Visitor.Components;

namespace Gof.Visitor.Visitors
{
    public interface IVisitor
    {
        void VisitConcreteComponentA(ConcreteComponentA element);

        void VisitConcreteComponentB(ConcreteComponentB element);
    }
}