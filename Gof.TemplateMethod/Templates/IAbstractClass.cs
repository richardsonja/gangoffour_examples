﻿namespace Gof.TemplateMethod.Templates
{
    public interface IAbstractClass
    {
        void TemplateMethod();
    }
}