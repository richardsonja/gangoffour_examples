﻿using System;
using Gof.TemplateMethod.Templates;

namespace Gof.TemplateMethod
{
    internal class Program
    {
        private static void Main()
        {
            Console.WriteLine("Same client code can work with different subclasses:");

            ClientCode(new ConcreteClass1());

            Console.Write("\n");

            Console.WriteLine("Same client code can work with different subclasses:");
            ClientCode(new ConcreteClass2());
        }

        public static void ClientCode(IAbstractClass abstractClass)
        {
            // ...
            abstractClass.TemplateMethod();
            // ...
        }
    }
}