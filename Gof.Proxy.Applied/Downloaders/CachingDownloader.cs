﻿using System;
using System.Collections.Generic;

namespace Gof.Proxy.Applied.Downloaders
{
    public class CachingDownloader : ICachingDownloader
    {
        private readonly Dictionary<Uri, string> _cache = new Dictionary<Uri, string>();
        private readonly ISimpleDownloader _downloader;

        public CachingDownloader(ISimpleDownloader downloader) => _downloader = downloader;

        public string Download(Uri url)
        {
            if (_cache.ContainsKey(url))
            {
                var results = _cache[url];
                Console.WriteLine("Found in cache...");
                Console.WriteLine($"Results were {results}.");
                return results;
            }

            Console.WriteLine("Not Found in cache, retrieving...");
            var result = _downloader.Download(url);
            _cache.Add(url, result);

            return result;
        }
    }
}