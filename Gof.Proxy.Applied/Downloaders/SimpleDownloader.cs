﻿using Gof.Proxy.Applied.RandomGenerators;
using System;

namespace Gof.Proxy.Applied.Downloaders
{
    public class SimpleDownloader : ISimpleDownloader
    {
        private readonly IRandomGenerator _generator;

        public SimpleDownloader(IRandomGenerator generator) => _generator = generator;

        public string Download(Uri url)
        {
            Console.WriteLine("Downloading a file from the internet.");
            var results = _generator.GenerateRandomAlphaNumericString(50);
            Console.WriteLine($"Downloaded. Results were {results}.");

            return results;
        }
    }
}