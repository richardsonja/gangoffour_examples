﻿using System;

namespace Gof.Proxy.Applied.Downloaders
{
    public interface IDownloader
    {
        string Download(Uri url);
    }
}