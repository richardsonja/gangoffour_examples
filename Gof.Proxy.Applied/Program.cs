﻿using Gof.Proxy.Applied.Downloaders;
using Gof.Proxy.Applied.RandomGenerators;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Gof.Proxy.Applied
{
    internal class Program
    {
        private static void ClientCode(IDownloader downloader)
        {
            var resultFromFirstCall = downloader.Download(new Uri("http://example.com/"));

            var resultFromSecondCall = downloader.Download(new Uri("http://example.com/"));
        }

        private static void Main()
        {
            //setup our DI
            var serviceProvider = new ServiceCollection()
                                  .AddTransient<ISimpleDownloader, SimpleDownloader>()
                                  .AddTransient<ICachingDownloader, CachingDownloader>()
                                  .AddTransient<IRandomGenerator, RandomGenerator>()
                                  .BuildServiceProvider();

            //do the actual work here
            var simpleDownloader = serviceProvider.GetService<ISimpleDownloader>();
            var cacheDownloader = serviceProvider.GetService<ICachingDownloader>();
            Console.WriteLine("***************Simple Downloader*************");
            ClientCode(simpleDownloader);

            Console.WriteLine("***************Cache Downloader*************");
            ClientCode(cacheDownloader);
        }
    }
}