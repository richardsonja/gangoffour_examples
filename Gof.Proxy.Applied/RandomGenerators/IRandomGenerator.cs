﻿namespace Gof.Proxy.Applied.RandomGenerators
{
    public interface IRandomGenerator
    {
        string GenerateRandomAlphaNumericString(int length);
    }
}