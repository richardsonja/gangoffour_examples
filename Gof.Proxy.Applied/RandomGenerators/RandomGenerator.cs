﻿using System;
using System.Linq;

namespace Gof.Proxy.Applied.RandomGenerators
{
    public class RandomGenerator : IRandomGenerator
    {
        private readonly Random _random = new Random();

        public string GenerateRandomAlphaNumericString(int length)
        {
            // https://stackoverflow.com/a/1344242
            // The use of the Random class makes this unsuitable for anything security related,
            // such as creating passwords or tokens.
            // Use the RNGCryptoServiceProvider class if you need a strong random number generator.
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(
                              Enumerable.Repeat(chars, length)
                                        .Select(s => s[_random.Next(s.Length)])
                                        .ToArray());
        }
    }
}