﻿using Gof.Factory.Applied.SocialNetworkPosters;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Gof.Factory.Applied
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Testing ConcreteCreator1:\n");
            ClientCode(new FacebookPoster("john_smith", "******"));
            Console.WriteLine();

            Console.WriteLine("Testing ConcreteCreator2:\n");
            ClientCode(new LinkedInPoster("john_smith@example.com", "******"));
        }

        private static void ClientCode(SocialNetworkPoster creator)
        {
            creator.Post("Hello world!");
            creator.Post("I had a large hamburger this morning!");
        }
    }
}