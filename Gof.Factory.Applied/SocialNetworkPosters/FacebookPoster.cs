﻿using System;
using System.Collections.Generic;
using System.Text;
using Gof.Factory.Applied.SocialNetworkConnectors;

namespace Gof.Factory.Applied.SocialNetworkPosters
{
    public class FacebookPoster : SocialNetworkPoster
    {
        private readonly string _login;
        private readonly string _password;

        public FacebookPoster(string login, string password)
        {
            _login = login;
            _password = password;
        }

        public override ISocialNetworkConnector GetSocialNetwork()
        {
           return new FacebookConnector(_login, _password);
        }
    }
}
