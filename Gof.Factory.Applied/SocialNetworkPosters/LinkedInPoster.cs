﻿using Gof.Factory.Applied.SocialNetworkConnectors;

namespace Gof.Factory.Applied.SocialNetworkPosters
{
    public class LinkedInPoster : SocialNetworkPoster
    {
        private readonly string _email;
        private readonly string _password;

        public LinkedInPoster(string email, string password)
        {
            _email = email;
            _password = password;
        }

        public override ISocialNetworkConnector GetSocialNetwork()
        {
            return new LinkedInConnector(_email, _password);
        }
    }
}