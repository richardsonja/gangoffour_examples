﻿using Gof.Factory.Applied.SocialNetworkConnectors;

namespace Gof.Factory.Applied.SocialNetworkPosters
{
    public abstract class SocialNetworkPoster
    {
        public abstract ISocialNetworkConnector GetSocialNetwork();

        public void Post(string content)
        {
            var network = GetSocialNetwork();

            network.LogIn();
            network.CreatePost(content);
            network.Logout();
        }
    }
}