﻿using System;

namespace Gof.Factory.Applied.SocialNetworkConnectors
{
    public class FacebookConnector : ISocialNetworkConnector
    {
        private readonly string _login;
        private readonly string _password;

        public FacebookConnector(string login, string password)
        {
            _login = login;
            _password = password;
        }

        public void CreatePost(string content)
        {
            Console.WriteLine("Send HTTP API requests to create a post in Facebook timeline.");
            Console.WriteLine(content);
        }

        public void LogIn()
        {
            Console.WriteLine($"Send HTTP API request to log in Facebook user {_login} with password {_password}\n");
        }

        public void Logout()
        {
            Console.WriteLine($"Send HTTP API request to log out Facebook user {_login}\n");
        }
    }
}