﻿using System;

namespace Gof.Factory.Applied.SocialNetworkConnectors
{
    public class LinkedInConnector : ISocialNetworkConnector
    {
        private readonly string _email;
        private readonly string _password;

        public LinkedInConnector(string email, string password)
        {
            _email = email;
            _password = password;
        }

        public void CreatePost(string content)
        {
            Console.WriteLine("Send HTTP API requests to create a post in LinkedIn timeline.");
            Console.WriteLine(content);
        }

        public void LogIn()
        {
            Console.WriteLine($"Send HTTP API request to log in LinkedIn user {_email} with password {_password}\n");
        }

        public void Logout()
        {
            Console.WriteLine($"Send HTTP API request to log out LinkedIn user {_email}\n");
        }
    }
}