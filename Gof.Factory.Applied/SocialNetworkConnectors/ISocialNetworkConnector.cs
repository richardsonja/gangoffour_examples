﻿namespace Gof.Factory.Applied.SocialNetworkConnectors
{
    public interface ISocialNetworkConnector
    {
        void CreatePost(string content);

        void LogIn();

        void Logout();
    }
}