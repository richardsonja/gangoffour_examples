﻿using Gof.Proxy.Subjects;
using System;

namespace Gof.Proxy
{
    public class Program
    {
        public static void ClientCode(ISubject subject)
        {
            // ...

            subject.Request();

            // ...
        }

        private static void Main()
        {
            Console.WriteLine("Client: Executing the client code with a real subject:");
            var realSubject = new RealSubject();
            ClientCode(realSubject);

            Console.WriteLine();

            Console.WriteLine("Client: Executing the same client code with a proxy:");
            var proxy = new ProxySubject(realSubject);
            ClientCode(proxy);
        }
    }
}