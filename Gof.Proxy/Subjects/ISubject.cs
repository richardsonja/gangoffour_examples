﻿namespace Gof.Proxy.Subjects
{
    public interface ISubject
    {
        void Request();
    }
}
