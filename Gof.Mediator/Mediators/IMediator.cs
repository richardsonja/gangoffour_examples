﻿namespace Gof.Mediator.Mediators
{
    public interface IMediator
    {
        void Notify(object sender, string ev);
    }
}