﻿using Gof.Mediator.Mediators;

namespace Gof.Mediator.Components
{
    public abstract class BaseComponent
    {
        protected IMediator Mediator;


        public void SetMediator(IMediator mediator)
        {
            Mediator = mediator;
        }
    }
}