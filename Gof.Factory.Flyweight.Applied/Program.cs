﻿using Gof.Factory.Flyweight.Applied.Flyweights;
using System.Collections.Generic;

namespace Gof.Factory.Flyweight.Applied
{
    internal class Program
    {
        private static void Main()
        {
            var database = new CatDatabase();

            database.AddCat(
                            "Bob",
                            "15",
                            "Jimmy",
                            "Made up",
                            "myspace.com/2323asdf.jpeg",
                            "red",
                            "rough",
                            "long hair",
                            "medium");

            var searchResult = database.FindCat(
                                                new List<KeyValuePair<string, string>>
                                                {
                                                    new KeyValuePair<string, string>("Breed", "Made up")
                                                });
            searchResult?.Render();
        }
    }
}