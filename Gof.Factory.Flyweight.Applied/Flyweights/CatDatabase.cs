﻿using Gof.Factory.Flyweight.Applied.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Gof.Factory.Flyweight.Applied.Flyweights
{
    public class CatDatabase
    {
        private readonly List<Cat> _cats = new List<Cat>();
        private readonly List<KeyValuePair<string, CatVariation>> _catVariations = new List<KeyValuePair<string, CatVariation>>();

        public void AddCat(
            string name,
            string age,
            string owner,
            string breed,
            string image,
            string color,
            string texture,
            string fur,
            string size)
        {
            var catVariation = GetVariation(
                                            breed,
                                            image,
                                            color,
                                            texture,
                                            fur,
                                            size);

            _cats.Add(new Cat(name, age, owner, catVariation));

            Console.WriteLine($"Added a cat ({name}, {breed})");
        }

        public Cat FindCat(IEnumerable<KeyValuePair<string, string>> query)
        {
            return _cats.FirstOrDefault(x => x.HasMatch(query));
        }

        private CatVariation GetVariation(
            string breed,
            string image,
            string color,
            string texture,
            string fur,
            string size)
        {
            var catVariation = new CatVariation(
                                                breed,
                                                image,
                                                color,
                                                texture,
                                                fur,
                                                size);
            var key = catVariation.GetKey();
            if (_catVariations.All(x => x.Key != key))
            {
                _catVariations.Add(new KeyValuePair<string, CatVariation>(key, catVariation));
            }

            return catVariation;
        }
    }
}