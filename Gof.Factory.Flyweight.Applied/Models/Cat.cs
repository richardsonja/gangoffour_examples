﻿using System.Collections.Generic;
using System.Linq;

namespace Gof.Factory.Flyweight.Applied.Models
{
    public class Cat
    {
        private readonly string _age;
        private readonly CatVariation _catVariation;
        private readonly string _name;
        private readonly string _owner;

        public Cat(string name, string age, string owner, CatVariation catVariation)
        {
            _name = name;
            _age = age;
            _owner = owner;
            _catVariation = catVariation;
        }

        public bool HasMatch(IEnumerable<KeyValuePair<string, string>> query)
        {
            return _catVariation.HasMatch(query)
                   || query.Any(x => x.Value == _name || x.Value == _age || x.Value == _owner);
        }

        public void Render()
        {
            _catVariation.Render(_name, _age, _owner);
        }
    }
}