﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Gof.Factory.Flyweight.Applied.Models
{
    public class CatVariation
    {
        private readonly List<KeyValuePair<string, string>> _details = new List<KeyValuePair<string, string>>();
        private const string _breed = "Breed";
        private const string _color = "Color";
        private const string _fur = "Fur";
        private const string _imageUrl = "Image Url";
        private const string _size = "Size";
        private const string _texture = "Texture";

        public CatVariation(string breed, string image, string color, string texture, string fur, string size)
        {
            _details.Add(new KeyValuePair<string, string>(_breed, breed));
            _details.Add(new KeyValuePair<string, string>(_imageUrl, image));
            _details.Add(new KeyValuePair<string, string>(_color, color));
            _details.Add(new KeyValuePair<string, string>(_texture, texture));
            _details.Add(new KeyValuePair<string, string>(_fur, fur));
            _details.Add(new KeyValuePair<string, string>(_size, size));
        }

        public string GetKey()
        {
            return string.Join(
                               '_',
                               _details
                                   .Select(x => x.Value)
                                   .Where(x => !string.IsNullOrEmpty(x)));
        }

        public bool HasMatch(IEnumerable<KeyValuePair<string, string>> query)
        {
            return query.Select(
                                keyValuePair => new
                                {
                                    keyValuePair,
                                    detail = _details.FirstOrDefault(x => keyValuePair.Key == x.Key)
                                })
                        .Where(t1 => t1.detail.Key != null)
                        .Select(t1 => t1.detail.Value == t1.keyValuePair.Value)
                        .FirstOrDefault();
        }

        public void Render(string name, string age, string owner)
        {
            Console.WriteLine($"==== {name} ====");
            Console.WriteLine($"Age: {age}");
            Console.WriteLine($"Owner: {owner}");
            Console.WriteLine($"Breed: {_breed}");
            _details.ForEach(x => Console.WriteLine($"{x.Key}: {x.Value}"));
            Console.WriteLine("====  ====");
        }
    }
}