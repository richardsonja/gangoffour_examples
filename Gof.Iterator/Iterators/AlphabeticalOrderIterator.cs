﻿using Gof.Iterator.Collections;

namespace Gof.Iterator.Iterators
{
    public class AlphabeticalOrderIterator : CustomInIterator<WordsCollection>
    {
        public AlphabeticalOrderIterator(WordsCollection collection, bool reverse = false)
            : base(collection, reverse)
        {
        }
    }
}