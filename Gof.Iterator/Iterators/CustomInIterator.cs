﻿using Gof.Iterator.Collections;

namespace Gof.Iterator.Iterators
{
    public class CustomInIterator<T> : Iterator where T : ICustomCollection
    {
        private readonly T _collection;

        private readonly bool _reverse;
        private int _position = -1;

        public CustomInIterator(T collection, bool reverse = false)
        {
            _collection = collection;
            _reverse = reverse;

            if (reverse) _position = collection.GetItems().Count;
        }

        public override object Current()
        {
            return _collection.GetItems()[_position];
        }

        public override int Key()
        {
            return _position;
        }

        public override bool MoveNext()
        {
            var updatedPosition = _position + (_reverse ? -1 : 1);

            if (updatedPosition >= 0 && updatedPosition < _collection.GetItems().Count)
            {
                _position = updatedPosition;
                return true;
            }

            return false;
        }

        public override void Reset()
        {
            _position = _reverse
                ? _collection.GetItems().Count - 1
                : 0;
        }
    }
}