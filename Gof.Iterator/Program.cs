﻿using System;
using Gof.Iterator.Collections;

namespace Gof.Iterator
{
    internal class Program
    {
        private static void Main()
        {
            WordsCollection();

            DayOfWeekCollection();
        }

        private static void DayOfWeekCollection()
        {
            Console.WriteLine();
            Console.WriteLine("using while loop");
            Console.WriteLine("Day of week collection:");
            var dayOfWeek = new DayOfWeekCollection();
            var dayOfWeekItterator = dayOfWeek.GetEnumerator();
            while (dayOfWeekItterator.MoveNext()) Console.WriteLine(dayOfWeekItterator.Current);

            Console.WriteLine();
            Console.WriteLine("using foreach");
            Console.WriteLine("REVERSE Day of week collection:");
            dayOfWeek.ReverseDirection();
            foreach (var element in dayOfWeek) Console.WriteLine(element);
        }

        private static void WordsCollection()
        {
            var collection = new WordsCollection();
            collection.AddItem("First");
            collection.AddItem("Second");
            collection.AddItem("Third");

            Console.WriteLine("Straight traversal:");

            foreach (var element in collection) Console.WriteLine(element);

            Console.WriteLine("\nReverse traversal:");

            collection.ReverseDirection();

            foreach (var element in collection) Console.WriteLine(element);
        }
    }
}