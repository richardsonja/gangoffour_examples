﻿using System.Collections;
using System.Collections.Generic;

namespace Gof.Iterator.Collections
{
    public interface ICustomCollection
    {
        void ReverseDirection();
        List<string> GetItems();
        void AddItem(string item);
        IEnumerator GetEnumerator();
    }
}