﻿using System;
using System.Collections;
using System.Collections.Generic;
using Gof.Iterator.Iterators;

namespace Gof.Iterator.Collections
{
    public class DayOfWeekCollection : IteratorAggregate, ICustomCollection
    {
        private readonly List<string> _dayOfWeeks = new List<string>
        {
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
            "Sunday"
        };

        private bool _direction;

        public void ReverseDirection()
        {
            _direction = !_direction;
        }

        public List<string> GetItems()
        {
            return _dayOfWeeks;
        }

        public void AddItem(string item)
        {
            throw new InvalidOperationException("This collection is locked down.  You can not add new items to it.");
        }

        public override IEnumerator GetEnumerator()
        {
            return new CustomInIterator<DayOfWeekCollection>(this, _direction);
        }
    }
}