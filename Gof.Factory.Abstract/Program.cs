﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace Gof.Factory.Abstract
{
    class Program
    {
        static void Main(string[] args)
        {
            //setup our DI
            var serviceProvider = new ServiceCollection()
             //   .AddTransient<IBuilder, ConcreteBuilder>()
             .BuildServiceProvider();


            //do the actual work here
            //  var director = serviceProvider.GetService<IDirector>();
            Console.WriteLine("Hello World!");
        }
    }
}
