﻿using System;

namespace Gof.Singleton.ThreadSafe.Singletons
{
    public class SingletonObject
    {
        private SingletonObject() { }

        private static SingletonObject _instance;

        // We now have a lock object that will be used to synchronize threads
        // during first access to the Singleton.
        private static readonly object Lock = new object();

        public static SingletonObject GetInstance(string value)
        {
            // This conditional is needed to prevent threads stumbling over the
            // lock once the instance is ready.
            if (_instance != null)
            {
                return _instance;
            }
            // Now, imagine that the program has just been launched. Since
            // there's no Singleton instance yet, multiple threads can
            // simultaneously pass the previous conditional and reach this
            // point almost at the same time. The first of them will acquire
            // lock and will proceed further, while the rest will wait here.
            lock (Lock)
            {
                // The first thread to acquire the lock, reaches this
                // conditional, goes inside and creates the Singleton
                // instance. Once it leaves the lock block, a thread that
                // might have been waiting for the lock release may then
                // enter this section. But since the Singleton field is
                // already initialized, the thread won't create a new
                // object.
                if (_instance == null)
                {
                    _instance = new SingletonObject
                    {
                        Value = value
                    };
                }
            }
            return _instance;
        }

        // We'll use this property to prove that our Singleton really works.
        public string Value { get; set; }

        public void SomeMessage()
        {
            // ...
            Console.WriteLine($"This is thread safe! here is your value {Value}");
        }
    }
}
