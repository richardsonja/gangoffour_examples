﻿using System;
using Gof.Memento.Caretakers;
using Gof.Memento.Originators;

namespace Gof.Memento
{
    public class Program
    {
        private static void Main(string[] args)
        {
            var originator = new Originator("Super-duper-super-puper-super.");
            var caretaker = new Caretaker(originator);

            caretaker.Backup();
            originator.DoSomething();

            caretaker.Backup();
            originator.DoSomething();

            caretaker.Backup();
            originator.DoSomething();

            Console.WriteLine();
            caretaker.ShowHistory();

            Console.WriteLine("\nClient: Now, let's rollback!\n");
            caretaker.Undo();

            Console.WriteLine("\n\nClient: Once more!\n");
            caretaker.Undo();

            Console.WriteLine();
        }
    }
}