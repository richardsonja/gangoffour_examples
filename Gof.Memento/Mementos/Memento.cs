﻿using System;

namespace Gof.Memento.Mementos
{
    public interface IMemento
    {
        string GetName();

        string GetState();

        DateTime GetDate();
    }
}