﻿using System.Collections.Generic;

namespace Gof.Prototype.Applied.Models
{
    public class Author
    {
        public string Name;

        public List<Page> Pages = new List<Page>();

        public Author(string name)
        {
            Name = name;
        }

        public Author AddToPage(Page page)
        {
            Pages.Add(page);
            return this;
        }

        public override string ToString()
        {
            return string.Join("\n", Pages);
        }
    }
}