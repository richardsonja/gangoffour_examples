﻿using System;
using System.Collections.Generic;

namespace Gof.Prototype.Applied.Models
{
    public class Page
    {
        public Author Author;
        public string Body;
        public List<string> Comments = new List<string>();
        public string Title;
        private readonly DateTime _createdDateTime;

        public Page(string title, string body, Author author)
        {
            Title = title;
            Body = body;
            Author = author;

            _createdDateTime = DateTime.Now;
            Comments = new List<string>();

            Author.AddToPage(this);
        }

        public Page AddComment(string comment)
        {
            Comments.Add(comment);
            return this;
        }

        public Page Clone()
        {
            return new Page($"Copy of {Title}", Body, Author);
        }

        public override string ToString()
        {
            return
                $"{Author.Name} created this page on {_createdDateTime.ToString()}.  Details: \n Title: {Title} \n \n {Body} \n\n Comments: \n {string.Join("\n", Comments)} \n";
        }
    }
}