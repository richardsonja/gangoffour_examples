﻿using Gof.Prototype.Applied.Models;
using System;

namespace Gof.Prototype.Applied
{
    public class Program
    {
        private static void Main(string[] args)
        {
            var author = new Author("John Smith");
            var page = new Page("Tip of the day", "Keep calm and code on.", author)
                .AddComment("Nice tip, thanks!");

            var draft1 = page.Clone();
            draft1.Body = "Please keep calm and keep coding on!";

            Console.WriteLine("**************************************************************************************");
            Console.WriteLine("Dumping the clone. note that the author is now referencing two objects");
            Console.WriteLine(draft1.Author.ToString());

            var draft2 = page.Clone();
            draft2.Body =
                "Also, pay attention to your cat! Otherwise they will walk across your kedfdgfhvjmk,l;yboard...too late.";

            Console.WriteLine("**************************************************************************************");
            Console.WriteLine("Dumping the clone. note that the author is now referencing tree objects");
            Console.WriteLine("NOTE: draft2 is cloning page, not draft1 but it still works!");
            Console.WriteLine(draft2.Author.ToString());
        }
    }
}