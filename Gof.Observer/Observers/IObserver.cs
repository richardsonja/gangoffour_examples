﻿using Gof.Observer.Subjects;

namespace Gof.Observer.Observers
{
    public interface IObserver
    {
        // Receive update from subject
        void Update(ISubject subject);
    }
}