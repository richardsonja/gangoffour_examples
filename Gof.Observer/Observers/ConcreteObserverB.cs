﻿using System;
using Gof.Observer.Subjects;

namespace Gof.Observer.Observers
{
    public class ConcreteObserverB : IObserver
    {
        public void Update(ISubject subject)
        {
            if (((Subject) subject).State != 1)
            {
                Console.WriteLine("ConcreteObserverB: Reacted to the event.");
            }
        }
    }
}