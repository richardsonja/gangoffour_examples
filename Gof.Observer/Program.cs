﻿using Gof.Observer.Observers;
using Gof.Observer.Subjects;

namespace Gof.Observer
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var subject = new Subject();
            var observerA = new ConcreteObserverA();
            subject.Attach(observerA);

            var observerB = new ConcreteObserverB();
            subject.Attach(observerB);

            subject.SomeBusinessLogic();
            subject.SomeBusinessLogic();
            subject.SomeBusinessLogic();
            subject.SomeBusinessLogic();
            subject.SomeBusinessLogic();
            subject.SomeBusinessLogic();
            subject.SomeBusinessLogic();

            subject.Detach(observerB);

            subject.SomeBusinessLogic();
        }
    }
}