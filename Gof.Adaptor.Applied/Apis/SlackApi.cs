﻿using System;

namespace Gof.Adapter.Applied.Apis
{
    public class SlackApi : ISlackApi
    {
        private readonly string _apiKey;
        private readonly string _login;

        public SlackApi(string login, string apiKey)
        {
            _login = login;
            _apiKey = apiKey;
        }

        public void LogIn()
        {
            Console.WriteLine($"Logged into a slack account using {_login}.");
        }

        public void SendMessage(string chatId, string message)
        {
            Console.WriteLine($"Posted the following message into '{chatId} chat: '{message}'.\n");
        }
    }
}