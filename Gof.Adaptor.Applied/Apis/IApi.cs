﻿namespace Gof.Adapter.Applied.Apis
{
    public interface IApi
    {
        void LogIn();
        void SendMessage(string chatId, string message);
    }
}