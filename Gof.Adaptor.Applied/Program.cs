﻿using Gof.Adapter.Applied.Apis;
using Gof.Adapter.Applied.Notifications;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Gof.Adapter.Applied
{
    public class Program
    {
        private static void ClientCode(INotification notification)
        {
            notification.Send(
                "Website is down!",
                "<strong style='color:red;font-size: 50px;'>Alert!</strong> Our website is not responding. Call admins and bring it up!");
        }

        private static void Main(string[] args)
        {
            //setup our DI
            var serviceProvider = new ServiceCollection()
                .AddTransient<IEmailNotification>(p => new EmailNotification("email@email.com"))
                .AddTransient<ISlackApi>(p => new SlackApi("slackLogin", new Guid().ToString()))
                .AddTransient<ISlackNotification>(p =>
                    new SlackNotification(p.GetRequiredService<ISlackApi>(), new Guid().ToString()))
                .BuildServiceProvider();

            //do the actual work here
            var emailNotification = serviceProvider.GetService<IEmailNotification>();
            var slackNotification = serviceProvider.GetService<ISlackNotification>();
            Console.WriteLine("************************email****************");
            ClientCode(emailNotification);

            Console.WriteLine("************************slack****************");
            ClientCode(slackNotification);
        }
    }
}