﻿namespace Gof.Adapter.Applied.Notifications
{
    public interface INotification
    {
        void Send(string title, string message);
    }
}