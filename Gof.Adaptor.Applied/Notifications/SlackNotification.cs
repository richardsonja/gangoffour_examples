﻿using Gof.Adapter.Applied.Apis;
using System.Net;

namespace Gof.Adapter.Applied.Notifications
{
    public class SlackNotification : ISlackNotification
    {
        private readonly string _chatId;
        private readonly ISlackApi _slack;

        public SlackNotification(ISlackApi slack, string chatId)
        {
            _slack = slack;
            _chatId = chatId;
        }

        public void Send(string title, string message)
        {
            var slackMessage = $"#{title}#{WebUtility.HtmlEncode(message)}";
            _slack.LogIn();
            _slack.SendMessage(_chatId, slackMessage);
        }
    }
}