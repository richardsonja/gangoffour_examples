﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gof.Builder.Applied.Builders
{
    public interface ISqlQueryBuilder
    {
        ISqlQueryBuilder Select(string table, IEnumerable<string> fields);

        ISqlQueryBuilder Where(string field, string value, string operatorValue = "=");

        ISqlQueryBuilder Limit(int start, int offset);

        string GetSql();
    }
}
