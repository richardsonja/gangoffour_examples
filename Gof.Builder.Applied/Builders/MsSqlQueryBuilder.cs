﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Gof.Builder.Applied.Models;

namespace Gof.Builder.Applied.Builders
{
    public class MsSqlQueryBuilder : IMsSqlQueryBuilder
    {
        protected Query Query = new Query();

        public virtual ISqlQueryBuilder Select(string table, IEnumerable<string> fields)
        {
            Query.QueryType = QueryType.Select;
            var columns = string.Join(", ", fields);
            Query.BaseQuery = $"SELECT {columns} FROM {table}";


            return this;
        }

        public virtual ISqlQueryBuilder Where(string field, string value, string operatorValue = "=")
        {
            if (!Query.QueryType.IsWhereStatementEligible())
            {
                throw new InvalidOperationException(
                    $"WHERE can only be added on {string.Join(", ", QueryTypeExtensions.WhereStatementEligibleCollection.Select(x => x.ToString()))}");
            }

            Query.WhereConditions ??= new List<Tuple<string, string, string>>();
            Query.WhereConditions.Add(new Tuple<string, string, string>(field, operatorValue, value));

            return this;
        }

        public virtual ISqlQueryBuilder Limit(int start, int offset)
        {
            if (!Query.QueryType.IsLimitStatementEligible())
            {
                throw new InvalidOperationException(
                    $"LIMIT can only be added to SELECT");
            }

            var offsetStatement = offset < 1
                ? string.Empty
                : $"OFFSET {offset}";
            Query.LimitQuery = $"LIMIT {start} {offsetStatement}";
            return this;
        }

        public string GetSql()
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.Append(Query.BaseQuery);
            stringBuilder.Append(" ");
            stringBuilder.Append(Query.ToWhereStatement());
            stringBuilder.Append(" ");
            stringBuilder.Append(Query.LimitQuery);
            return stringBuilder.ToString();
        }

        protected void Reset()
        {
            Query = new Query();
        }
    }
}