﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Gof.Builder.Applied.Models;

namespace Gof.Builder.Applied.Builders
{
    public class MySqlQueryBuilder : MsSqlQueryBuilder,IMySqlQueryBuilder
    {
        

        public override ISqlQueryBuilder Limit(int start, int offset)
        {
            if (!Query.QueryType.IsLimitStatementEligible())
            {
                throw new InvalidOperationException(
                    $"LIMIT can only be added to SELECT");
            }

            Query.LimitQuery = $"LIMIT {start}, {offset}";
            return this;
        }

    }
}