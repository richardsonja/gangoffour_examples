﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using Gof.Builder.Applied.Builders;

namespace Gof.Builder.Applied
{
    class Program
    {
        static void Main(string[] args)
        {
            //setup our DI
            var serviceProvider = new ServiceCollection()
                .AddTransient<IMsSqlQueryBuilder, MsSqlQueryBuilder>()
                .AddTransient<IMySqlQueryBuilder, MySqlQueryBuilder>()
             .BuildServiceProvider();


            //do the actual work here
            //  var director = serviceProvider.GetService<IDirector>();

            Console.WriteLine("MsSQL query Builder");
            ClientCode(serviceProvider.GetService<IMsSqlQueryBuilder>());

            Console.WriteLine("MySQL's Version of query Builder");
            ClientCode(serviceProvider.GetService<IMySqlQueryBuilder>());
        }

        public static void ClientCode(ISqlQueryBuilder queryBuilder)
        {
            var query = queryBuilder
                .Select("users", new List<string> {"name", "email", "password"})
                .Where("age", "18", ">")
                .Where("age", "30", "<")
                .Limit(10, 20);

            Console.WriteLine(query.GetSql());
        }
    }
}
