﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gof.Builder.Applied.Models
{
    public class Query
    {
        public string BaseQuery { get; set; }

        public QueryType QueryType { get; set;  }

        public List<Tuple<string, string, string>> WhereConditions = new List<Tuple<string, string, string>>();

        public string ToWhereStatement() =>
            string.Join(" AND ", WhereConditions.Select(x => $"{x.Item1} {x.Item2} {x.Item3}"));

        public string LimitQuery { get; set; }
    }
}
