﻿namespace Gof.Builder.Applied.Models
{
    public enum QueryType
    {
        Select,
        Update,
        Delete,
        Insert
    }
}