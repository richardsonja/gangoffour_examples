﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gof.Builder.Applied.Models
{
    public static class QueryTypeExtensions
    {
        public static readonly IReadOnlyCollection<QueryType> WhereStatementEligibleCollection= new List<QueryType>
        {
            QueryType.Select,
            QueryType.Delete,
            QueryType.Update
        };

        public static bool IsWhereStatementEligible(this QueryType queryType)
        {
            return WhereStatementEligibleCollection.Contains(queryType);
        }

        public static bool IsLimitStatementEligible(this QueryType queryType)
        {
            return queryType == QueryType.Select;
        }
    }
}
