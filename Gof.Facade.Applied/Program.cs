﻿using Gof.Facade.Applied.Facades;
using Gof.Facade.Applied.Subsystems;
using System;

namespace Gof.Facade.Applied
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var mortgage = new Mortgage();
            var customer = new Customer("Bob Silly");
            var eligible = mortgage.IsEligible(customer, 100000);

            Console.WriteLine($"{customer.Name} has been {(eligible ? "Approved" : "Rejected")}");
        }
    }
}