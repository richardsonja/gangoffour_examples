﻿using Gof.Facade.Applied.Subsystems;
using System;

namespace Gof.Facade.Applied.Facades
{
    public class Mortgage
    {
        private readonly Bank _bank = new Bank();
        private readonly Credit _credit = new Credit();
        private readonly Loan _loan = new Loan();

        public bool IsEligible(Customer customer, int amount)
        {
            Console.WriteLine(
                              "{0} applies for {1:C} loan\n",
                              customer.Name,
                              amount);

            var eligible = !(!_bank.HasSufficientSavings(customer, amount)
                             || !_loan.HasNoBadLoans(customer)
                             || !_credit.HasGoodCredit(customer));

            return eligible;
        }
    }
}