﻿using System.Collections.Generic;

namespace gof.builder.Products
{
    public class Product
    {
        private readonly List<object> _parts = new List<object>();

        public void Add(string part)
        {
            _parts.Add(part);
        }

        public string ListParts()
        {
            var str = string.Join(", ", _parts);

            return "Product parts: " + str + "\n";
        }
    }
}