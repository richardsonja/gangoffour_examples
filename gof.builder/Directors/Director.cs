﻿using gof.builder.Builders;
using gof.builder.Products;

namespace gof.builder.Directors
{
    public class Director : IDirector
    {
        private readonly IBuilder _builder;

        public Director(IBuilder builder)
        {
            _builder = builder;
        }

        public void BuildMinimalViableProduct()
        {
            _builder.BuildPartA();
        }

        public void BuildFullFeaturedProduct()
        {
            _builder.BuildPartA();
            _builder.BuildPartB();
            _builder.BuildPartC();
        }

        public Product GetProduct()
        {
            return _builder.GetProduct();
        }
    }
}