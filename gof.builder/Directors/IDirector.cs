﻿using gof.builder.Products;

namespace gof.builder.Directors
{
    public interface IDirector
    {
        void BuildMinimalViableProduct();
        void BuildFullFeaturedProduct();

        Product GetProduct();
    }
}