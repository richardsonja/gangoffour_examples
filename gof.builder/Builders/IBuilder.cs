﻿using gof.builder.Products;

namespace gof.builder.Builders
{
    public interface IBuilder
    {
        void BuildPartA();

        void BuildPartB();

        void BuildPartC();

        Product GetProduct();
    }
}