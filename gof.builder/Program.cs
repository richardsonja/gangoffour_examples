﻿using System;
using gof.builder.Builders;
using gof.builder.Directors;
using Microsoft.Extensions.DependencyInjection;

namespace gof.builder
{
    internal class Program
    {
        private static void Main()
        {
            //setup our DI
            var serviceProvider = new ServiceCollection()
                .AddTransient<IBuilder, ConcreteBuilder>()
                .AddTransient<IDirector, Director>()
                .BuildServiceProvider();


            //do the actual work here
            var director = serviceProvider.GetService<IDirector>();
            director.BuildMinimalViableProduct();
            Console.WriteLine("Standard basic product:");
            Console.WriteLine(director.GetProduct().ListParts());


            director.BuildFullFeaturedProduct();
            Console.WriteLine("Standard full featured product:");
            Console.WriteLine(director.GetProduct().ListParts());
        }
    }
}