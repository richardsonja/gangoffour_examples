﻿using Gof.State.Contexts;
using Gof.State.States;

namespace Gof.State
{
    internal class Program
    {
        private static void Main()
        {
            var context = new Context(new ConcreteStateA());
            context.Request1(); //a
            context.Request1(); // b
            context.Request2(); // b
            context.Request2(); // a
        }
    }
}