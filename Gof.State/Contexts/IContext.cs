﻿using Gof.State.States;

namespace Gof.State.Contexts
{
    public interface IContext
    {
        void TransitionTo(IState state);
        void Request1();
        void Request2();
    }
}