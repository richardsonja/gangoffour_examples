﻿using System;
using Gof.State.States;

namespace Gof.State.Contexts
{
    public class Context : IContext
    {
        // A reference to the current state of the Context.
        private IState _state;

        public Context(IState state)
        {
            TransitionTo(state);
        }

        // The Context allows changing the State object at runtime.
        public void TransitionTo(IState state)
        {
            Console.WriteLine($"Context: Transition to {state.GetType().Name}.");
            _state = state;
            _state.SetContext(this);
        }

        // The Context delegates part of its behavior to the current State
        // object.
        public void Request1()
        {
            _state.Handle1();
        }

        public void Request2()
        {
            _state.Handle2();
        }
    }
}