﻿using Gof.State.Contexts;

namespace Gof.State.States
{
    public abstract class State : IState
    {
        protected IContext Context;

        public void SetContext(IContext context)
        {
            Context = context;
        }

        public abstract void Handle1();

        public abstract void Handle2();
    }
}