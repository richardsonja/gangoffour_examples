﻿using Gof.State.Contexts;

namespace Gof.State.States
{
    public interface IState
    {
        void SetContext(IContext context);
        void Handle1();
        void Handle2();
    }
}