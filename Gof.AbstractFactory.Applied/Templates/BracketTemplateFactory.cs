﻿using Gof.AbstractFactory.Applied.Templates.Pages;
using Gof.AbstractFactory.Applied.Templates.Renderers;

namespace Gof.AbstractFactory.Applied.Templates
{
    public class BracketTemplateFactory : IBracketTemplateFactory
    {
        private readonly IBracketPageTemplate _bracketPageTemplate;
        private readonly IBracketRenderer _bracketRenderer;

        public BracketTemplateFactory(
            IBracketPageTemplate bracketPageTemplate,
            IBracketRenderer bracketRenderer)
        {
            _bracketPageTemplate = bracketPageTemplate;
            _bracketRenderer = bracketRenderer;
        }

        public IPageTemplate CreatePageTemplate()
        {
            return _bracketPageTemplate;
        }

        public ITemplateRenderer GetRenderer()
        {
            return _bracketRenderer;
        }
    }
}