﻿using System.Collections.Generic;
using System.Linq;

namespace Gof.AbstractFactory.Applied.Templates.Renderers
{
    public class BracketRenderer : IBracketRenderer
    {
        public string Render(string templateString, IEnumerable<KeyValuePair<string, string>> args)
        {
            return args.Aggregate(templateString, FormatAndReplace);
        }

        private static string FormatAndReplace(string templateString, KeyValuePair<string, string> arg)
        {
            return templateString.Replace("{{" + arg.Key + "}}", arg.Value)
                .Replace("{{ " + arg.Key + " }}", arg.Value)
                .Replace("{{ " + arg.Key + "}}", arg.Value)
                .Replace("{{" + arg.Key + " }}", arg.Value);
        }
    }
}