﻿using System.Collections.Generic;

namespace Gof.AbstractFactory.Applied.Templates.Renderers
{
    public interface ITemplateRenderer
    {
        public string Render(string templateString, IEnumerable<KeyValuePair<string, string>> args);
    }
}