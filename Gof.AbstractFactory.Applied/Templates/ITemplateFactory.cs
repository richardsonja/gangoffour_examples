﻿using Gof.AbstractFactory.Applied.Templates.Pages;
using Gof.AbstractFactory.Applied.Templates.Renderers;

namespace Gof.AbstractFactory.Applied.Templates
{
    public interface ITemplateFactory
    {
        public IPageTemplate CreatePageTemplate();

        public ITemplateRenderer GetRenderer();
    }
}