﻿namespace Gof.AbstractFactory.Applied.Templates.Titles
{
    public interface ITitleTemplate
    {
        public string GetTitleString();
    }
}