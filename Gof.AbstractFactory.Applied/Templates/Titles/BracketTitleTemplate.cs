﻿namespace Gof.AbstractFactory.Applied.Templates.Titles
{
    public class BracketTitleTemplate : IBracketTitleTemplate
    {
        public string GetTitleString()
        {
            return "<h1>{{ title }}</h1>";
        }
    }
}