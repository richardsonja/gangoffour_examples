﻿using Gof.AbstractFactory.Applied.Templates.Titles;

namespace Gof.AbstractFactory.Applied.Templates.Pages
{
    public abstract class BasePageTemplate : IPageTemplate
    {
        protected ITitleTemplate TitleTemplate;

        protected BasePageTemplate(ITitleTemplate titleTemplate)
        {
            TitleTemplate = titleTemplate;
        }

        public abstract string GetPageTemplate();
    }
}