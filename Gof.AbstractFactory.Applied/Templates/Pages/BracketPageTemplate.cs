﻿using Gof.AbstractFactory.Applied.Templates.Titles;

namespace Gof.AbstractFactory.Applied.Templates.Pages
{
    public class BracketPageTemplate : BasePageTemplate, IBracketPageTemplate
    {
        public BracketPageTemplate(IBracketTitleTemplate bracketTitleTemplate) : base(bracketTitleTemplate)
        {
        }

        public override string GetPageTemplate()
        {
            var renderedTitle = TitleTemplate.GetTitleString();
            const string content = "{{ content }}";

            return $"<div class=\"page\">{renderedTitle}<article class=\"content\">{content}</article></div>";
        }
    }
}