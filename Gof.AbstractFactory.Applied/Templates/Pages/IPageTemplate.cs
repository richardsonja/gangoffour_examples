﻿namespace Gof.AbstractFactory.Applied.Templates.Pages
{
    public interface IPageTemplate
    {
        public string GetPageTemplate();
    }
}