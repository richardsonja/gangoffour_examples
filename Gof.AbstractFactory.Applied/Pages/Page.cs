﻿using Gof.AbstractFactory.Applied.Templates;
using System.Collections.Generic;

namespace Gof.AbstractFactory.Applied.Pages
{
    public class Page
    {
        public Page(string title, string content)
        {
            Title = title;
            Content = content;
        }

        public string Content { get; set; }
        public string Title { get; set; }

        public string Render(ITemplateFactory factory)
        {
            var pageTemplate = factory.CreatePageTemplate();
            var renderer = factory.GetRenderer();
            var args = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("title", Title),
                new KeyValuePair<string, string>("content", Content)
            };

            return renderer.Render(pageTemplate.GetPageTemplate(), args);
        }
    }
}