﻿using Gof.AbstractFactory.Applied.Pages;
using Gof.AbstractFactory.Applied.Templates;
using Microsoft.Extensions.DependencyInjection;
using System;
using Gof.AbstractFactory.Applied.Templates.Pages;
using Gof.AbstractFactory.Applied.Templates.Renderers;
using Gof.AbstractFactory.Applied.Templates.Titles;

namespace Gof.AbstractFactory.Applied
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //setup our DI
            var serviceProvider = new ServiceCollection()
                .AddTransient<IBracketTitleTemplate, BracketTitleTemplate>()
                .AddTransient<IBracketPageTemplate, BracketPageTemplate>()
                .AddTransient<IBracketRenderer, BracketRenderer>()
                .AddTransient<IBracketTemplateFactory, BracketTemplateFactory>()
                .BuildServiceProvider();

            //do the actual work here
             var templateFactory = serviceProvider.GetService<IBracketTemplateFactory>();

            var page = new Page("Title Is Here", "Yeap, you did it.");

            Console.WriteLine(page.Render(templateFactory));
        }
    }
}