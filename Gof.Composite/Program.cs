﻿using System;
using Gof.Composite.Components;

namespace Gof.Composite
{
    class Program
    {
        static void Main()
        {
            var leaf = new Leaf();
            Console.WriteLine("Client: I get a simple component:");
            ClientCode(leaf);

            // ...as well as the complex composites.
            var tree = new Components.Composite();
            var branch1 = new Components.Composite();
            branch1.Add(new Leaf());
            branch1.Add(new Leaf());

            var branch2 = new Components.Composite();
            branch2.Add(new Leaf());

            tree.Add(branch1);
            tree.Add(branch2);
            Console.WriteLine("Client: Now I've got a composite tree:");
            ClientCode(tree);

            Console.Write("Client: I don't need to check the components classes even when managing the tree:\n");
            ClientCode2(tree, leaf);
        }

        public static void ClientCode(IComponent leaf)
        {
            Console.WriteLine($"RESULT: {leaf.Operation()}\n");
        }

        public static void ClientCode2(IComponent component1, IComponent component2)
        {
            if (component1.IsComposite())
            {
                component1.Add(component2);
            }

            Console.WriteLine($"RESULT: {component1.Operation()}");
        }
    }
}
