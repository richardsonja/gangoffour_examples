﻿namespace Gof.Composite.Components
{
   public class Leaf : IComponent
    {
        public string Operation()
        {
            return "Leaf";
        }

        public  bool IsComposite()
        {
            return false;
        }
    }
}
