﻿using System;

namespace Gof.Composite.Components
{
    public interface IComponent
    {
       string Operation();
        public virtual void Add(IComponent component)
        {
            throw new NotImplementedException();
        }
        public  void Remove(IComponent component)
        {
            throw new NotImplementedException();
        }
        public  bool IsComposite()
        {
            return true;
        }
    }
}
