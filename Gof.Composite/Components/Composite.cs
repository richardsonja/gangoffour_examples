﻿using System.Collections.Generic;

namespace Gof.Composite.Components
{
    class Composite : IComponent
    {
        protected List<IComponent> Children = new List<IComponent>();

        public void Add(IComponent component)
        {
            this.Children.Add(component);
        }

        public void Remove(IComponent component)
        {
            this.Children.Remove(component);
        }

        // The Composite executes its primary logic in a particular way. It
        // traverses recursively through all its children, collecting and
        // summing their results. Since the composite's children pass these
        // calls to their children and so forth, the whole object tree is
        // traversed as a result.
        public string Operation()
        {
            var i = 0;
            var result = "Branch(";

            foreach (var component in this.Children)
            {
                result += component.Operation();
                if (i != this.Children.Count - 1)
                {
                    result += "+";
                }

                i++;
            }

            return result + ")";
        }
    }
}
