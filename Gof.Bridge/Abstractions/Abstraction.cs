﻿using Gof.Bridge.Implementations;

namespace Gof.Bridge.Abstractions
{
    public class Abstraction
    {
        protected IImplementation Implementation;

        public Abstraction(IImplementation implementation)
        {
            Implementation = implementation;
        }

        public virtual string Operation()
        {
            return "Abstract: Base operation with:\n" +
                   Implementation.OperationImplementation();
        }
    }
}