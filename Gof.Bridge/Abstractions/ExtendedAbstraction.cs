﻿using Gof.Bridge.Implementations;

namespace Gof.Bridge.Abstractions
{
    public class ExtendedAbstraction : Abstraction
    {
        public ExtendedAbstraction(IImplementation implementation) : base(implementation)
        {
        }

        public override string Operation()
        {
            return "ExtendedAbstraction: Extended operation with:\n" +
                   Implementation.OperationImplementation();
        }
    }
}