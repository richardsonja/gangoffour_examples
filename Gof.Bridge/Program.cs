﻿using System;
using Gof.Bridge.Abstractions;
using Gof.Bridge.Implementations;

namespace Gof.Bridge
{
    class Program
    {
        public static void Main()
        {
            ClientCode(new Abstraction(new ConcreteImplementationA()));
            ClientCode(new ExtendedAbstraction(new ConcreteImplementationB()));

        }

        public static void ClientCode(Abstraction abstraction)
        {
            Console.Write(abstraction.Operation());
        }
    }
}
