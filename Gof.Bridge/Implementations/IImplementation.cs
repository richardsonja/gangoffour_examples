﻿namespace Gof.Bridge.Implementations
{
    public interface IImplementation
    {
        string OperationImplementation();
    }
}
