﻿namespace Gof.Bridge.Applied.Models
{
    public class Product
    {
        protected string Description;
        protected string ImageUrl;
        protected decimal Price;
        protected string ProductId;
        protected string Title;

        public Product(string productId, string title, string description, string imageUrl, decimal price)
        {
            ProductId = productId;
            Title = title;
            Description = description;
            ImageUrl = imageUrl;
            Price = price;
        }

        public string GetDescription() => Description;

        public string GetImageUrl() => ImageUrl;

        public decimal GetPrice() => Price;

        public string GetProductId() => ProductId;

        public string GetTitle() => Title;
    }
}