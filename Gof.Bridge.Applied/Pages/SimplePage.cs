﻿using Gof.Bridge.Applied.Renderers;
using System.Collections.Generic;

namespace Gof.Bridge.Applied.Pages
{
    public class SimplePage : PageBase
    {
        protected string Content;

        protected string Title;

        public SimplePage(IRenderer renderer, string title, string content) : base(renderer)
        {
            Title = title;
            Content = content;
        }

        public override string View() =>
            Renderer.RenderParts(new List<string>
            {
                Renderer.RenderHeader(),
                Renderer.RenderTitle(Title),
                Renderer.RenderTextBlock(Content),
                Renderer.RenderFooter()
            });
    }
}