﻿using Gof.Bridge.Applied.Models;
using Gof.Bridge.Applied.Renderers;
using System.Collections.Generic;

namespace Gof.Bridge.Applied.Pages
{
    public class ProductPage : PageBase
    {
        protected Product Product;

        public ProductPage(IRenderer Renderer, Product product) : base(Renderer) => Product = product;

        public override string View() =>
            Renderer.RenderParts(new List<string>
            {
                Renderer.RenderHeader(),
                Renderer.RenderTitle(Product.GetTitle()),
                Renderer.RenderTextBlock(Product.GetDescription()),
                Renderer.RenderImage(Product.GetImageUrl()),
                Renderer.RenderLink("/cart/add/" + Product.GetProductId(), "Add to cart"),
                Renderer.RenderFooter()
            });
    }
}