﻿using Gof.Bridge.Applied.Renderers;

namespace Gof.Bridge.Applied.Pages
{
    public abstract class PageBase : IPage
    {
        protected IRenderer Renderer;

        protected PageBase(IRenderer renderer)
        {
            ChangeRenderer(renderer);
        }

        public void ChangeRenderer(IRenderer renderer)
        {
            Renderer = renderer;
        }

        public abstract string View();
    }
}