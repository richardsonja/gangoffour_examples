﻿using Gof.Bridge.Applied.Renderers;

namespace Gof.Bridge.Applied.Pages
{
    public interface IPage
    {
        void ChangeRenderer(IRenderer renderer);

        string View();
    }
}