﻿using System.Collections.Generic;

namespace Gof.Bridge.Applied.Renderers
{
    public interface IRenderer
    {
        string RenderFooter();

        string RenderHeader();

        string RenderImage(string url);

        string RenderLink(string url, string title);

        string RenderParts(IEnumerable<string> parts);

        string RenderTextBlock(string text);

        string RenderTitle(string title);
    }
}