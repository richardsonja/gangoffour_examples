﻿using System.Collections.Generic;

namespace Gof.Bridge.Applied.Renderers
{
    public class HtmlRenderer : IHtmlRenderer
    {
        public string RenderFooter() => "</body></html>";

        public string RenderHeader() => "<html><body>";

        public string RenderImage(string url) => $"<img src='{url}'>";

        public string RenderLink(string url, string title) => $"<a href='{url}'>{title}</a>";

        public string RenderParts(IEnumerable<string> parts) => string.Join("\n", parts);

        public string RenderTextBlock(string text) => $"<div class='text'>{text}</div>";

        public string RenderTitle(string title) => $"<h1>{title}</h1>";
    }
}