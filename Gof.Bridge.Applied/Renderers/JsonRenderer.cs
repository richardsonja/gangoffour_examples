﻿using System.Collections.Generic;

namespace Gof.Bridge.Applied.Renderers
{
    public class JsonRenderer : IJsonRenderer
    {
        public string RenderFooter() => "";

        public string RenderHeader() => "";

        public string RenderImage(string url) => $"\"img\" : \"{url}\"";

        public string RenderLink(string url, string title) => $"\"link\": {{\"href\": \"{url}\", \"title\": \"{title}\"\"}}";

        public string RenderParts(IEnumerable<string> parts) => $"{{ \n {string.Join("\n", parts)} \n}}";

        public string RenderTextBlock(string text) => $"\"text\" : \"{text}\"";

        public string RenderTitle(string title) => $"\"title\" : \"{title}\"";
    }
}