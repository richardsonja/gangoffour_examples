﻿using Gof.Bridge.Applied.Models;
using Gof.Bridge.Applied.Pages;
using Gof.Bridge.Applied.Renderers;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Gof.Bridge.Applied
{
    public class Program
    {
        private static void ClientCode(IPage page)
        {
            Console.WriteLine(page.View());
            Console.WriteLine();
            Console.WriteLine();
        }

        private static void Main(string[] args)
        {
            //setup our DI
            var serviceProvider = new ServiceCollection()
                .AddTransient<IJsonRenderer, JsonRenderer>()
                .AddTransient<IHtmlRenderer, HtmlRenderer>()
                .BuildServiceProvider();

            //do the actual work here
            var jsonRenderer = serviceProvider.GetService<IJsonRenderer>();
            var htmlRenderer = serviceProvider.GetService<IHtmlRenderer>();

            var simplePage = new SimplePage(htmlRenderer, "Home", "Welcome to our website!");
            Console.WriteLine("Html view of a simple page:");
            ClientCode(simplePage);

            Console.WriteLine("Json view of a simple page:");
            simplePage.ChangeRenderer(jsonRenderer);
            ClientCode(simplePage);

            var product = new Product("123", "Star Wars, episode1",
                "A long time ago in a galaxy far, far away...",
                "/images/star-wars.jpeg", 39.95M);
            var productPage = new ProductPage(htmlRenderer, product);
            Console.WriteLine("Html view of a product page:");
            ClientCode(productPage);

            Console.WriteLine("Json view of a product page:");
            productPage.ChangeRenderer(jsonRenderer);
            ClientCode(productPage);
        }
    }
}